#! /usr/bin/python3

import pandas
import numpy as np
from sklearn.cluster import KMeans
from scipy.spatial.distance import euclidean

raw_data = pandas.read_csv('data.csv', header=0)
# target = ['car','fad','mas','gla','con','adi']
# n_cluster = len(target)
n_cluster = 6

data = []
for index, row in raw_data.iterrows():
#     print(row)
    data.append([index, row['I0'], row['PA500'], row['HFS'], row['DA'], row['Area'], row['A/DA'], row['Max IP'], row['DR'], row['P']])

data = np.array(data)
data_with_idx = data[:]
data = data[:,1:]

def start_experiment(n_cluster, n_init, max_iter):
    if (len(n_cluster) == len(n_init) == len(max_iter)):
        n_experiment = len(n_cluster)
        iter_exp = 0
        while (iter_exp < n_experiment):

            kmeans = KMeans(n_clusters=n_cluster[iter_exp], init='random', n_init=n_init[iter_exp], max_iter=max_iter[iter_exp])
            kmeans.fit(data)
            result = kmeans.labels_

            grouped = {}
            for i in range(n_cluster[iter_exp]):
                grouped[i] = {}

            iter_data = 0
            for label in result:
                grouped[label][int(data_with_idx[iter_data][0])] = data_with_idx[iter_data][1:]
                iter_data += 1

            sil_sample = silhouette_samples(grouped, n_cluster[iter_exp], result)
            sil_score = silhouette_score(sil_sample)
            # print(sil_sample)
            print(sil_score)

            iter_exp += 1
    else:
        print("Dimensi parameter harus sama.")

def silhouette_samples(grouped_data, n_cluster, labels):
    sample_idx = 0
    sh_cof_all = []
    for y in labels:
        selected_cluster = grouped_data[y]
        n_data_in_cluster = len(selected_cluster)
        if n_data_in_cluster > 1:
            total_dist = 0
            for idx, data_cluster in selected_cluster.items():
                if idx != sample_idx:
                    total_dist += euclidean(selected_cluster[sample_idx], data_cluster)
            avg_within_cluster =  total_dist / (n_data_in_cluster-1)

            min_avg = None
            min_avg_cluster_idx = 0
            for iter_cluster in range(n_cluster):
                if iter_cluster != y:
                    total_dist = 0
                    for _, d in grouped_data[iter_cluster].items():
                        total_dist += euclidean(selected_cluster[sample_idx], d)
                    avg_other_cluster = total_dist / len(grouped_data[iter_cluster])

                    if (min_avg == None):
                        min_avg = avg_other_cluster
                    else:
                        if (min_avg > avg_other_cluster):
                            min_avg = avg_other_cluster

            sh_cof = (min_avg - avg_within_cluster) / max(min_avg, avg_within_cluster)
        else:
            sh_cof = 0
        sh_cof_all.append(sh_cof)
        sample_idx += 1
    return np.array(sh_cof_all)

def silhouette_score(silhouette_samples):
    total = 0
    for sil_sample in silhouette_samples:
        total += sil_sample
    return total / len(silhouette_samples)

n_clusters = [6, 6, 6, 6]
n_inits = [300, 500, 600, 700]
max_iter = [500, 500, 500, 500]
print('Eksperimen dengan jumlah inisialisasi yang berbeda : ')
start_experiment(n_clusters, n_inits, max_iter)

n_clusters = [2, 3, 5, 6]
n_inits = [500, 500, 500, 500]
max_iter = [500, 500, 500, 500]
print('Eksperimen degan jumlah kluster yang berbeda : ')
start_experiment(n_clusters, n_inits, max_iter)
